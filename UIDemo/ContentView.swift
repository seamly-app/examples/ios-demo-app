//
//  ContentView.swift
//  UIDemo
//
//  Created by Joran Kapteijns on 03/07/2020.
//  Copyright © 2020 Conversationals. All rights reserved.
//
// This file shows a simple setup to communicate with the Seamly chatbot
// To demonstrate working navigation, the iOS tutorial 'Master-Detail' is used as a basis
// The relevant portion of the code is highlighted in comments below.

import SwiftUI
import JavaScriptCore
import WebKit

struct ContentView: View {
    var body: some View {
        NavigationView {
            MasterView()
                .navigationBarTitle(Text("Seamly UI Demo"))
            DetailView()
        }.navigationViewStyle(DoubleColumnNavigationViewStyle())
    }
}

struct MasterView: View {
    var body: some View {
        List {

                NavigationLink(
                    destination: DetailView()
                ) {
                    Text("Start or restart a chat")
                }

                Button("Clear data") {
                    UserDefaults.standard.removeObject(forKey: "Seamly")

                }
        }
    }
}

struct DetailView: View {
    // Connect to your hosted web page here
    @ObservedObject var model = WebViewModel(link: "https://developers.seamly.ai/clients/web-ui/demos/app/index.html")
    
    var body: some View {
        SwiftUIWebView(viewModel: model).navigationBarTitle(Text("Chat"))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

class WebViewModel: ObservableObject {
    @Published var link: String
    @Published var didFinishLoading: Bool = false

    init (link: String) {
        self.link = link
    }
}

struct SwiftUIWebView: UIViewRepresentable {
    @ObservedObject var viewModel: WebViewModel

    let webView = WKWebView()
    
    func makeUIView(context: UIViewRepresentableContext<SwiftUIWebView>) -> WKWebView {
        self.webView.navigationDelegate = context.coordinator
        if let url = URL(string: "\(viewModel.link)?platform=ios") {
            self.webView.load(URLRequest(url: url))
        }
        
        // Connect the seamlyBridge coordinater, defined below. This line is required.
        webView.configuration.userContentController.add(context.coordinator, name: "seamlyBridge")
        
        // This demo app uses UserDefaults as a simple data storage mechanism.
        // You can connect to your own preferred data storage here.
        let defaults = UserDefaults.standard
        var bridgeSource: String;
        if let storedData = defaults.string(forKey: "Seamly") {
            // Communicate the stored data to JavaScript on initialisation.
            // When changing data storage, it would be easiest to keep these lines
            // and substitute your own variable.
            // If you want to make other changes here, for instance to combine JavaScript,
            // be sure to keep the naming ('window.seamlyBridge') and data format intact
            bridgeSource="window.seamlyBridgeData=" + storedData
        } else {
            bridgeSource="window.seamlyBridgeData={}"
        }

        // We also add the seamly-ui initialization code so
        // we ensure the UI will always be initialized after
        // the bridge data has been set.
        bridgeSource = bridgeSource + ";window.seamly = window.seamly || []; window.seamly.push({action: 'init'});"

        let bridgeScript = WKUserScript(source: bridgeSource, injectionTime: .atDocumentStart, forMainFrameOnly: false)
        webView.configuration.userContentController.addUserScript(bridgeScript)
        
        return self.webView
    }

    func updateUIView(_ uiView: WKWebView, context: UIViewRepresentableContext<SwiftUIWebView>) {
        return
    }

    class Coordinator: NSObject, WKNavigationDelegate, WKScriptMessageHandler {
        private var viewModel: WebViewModel
        var parent: SwiftUIWebView

        init(_ viewModel: WebViewModel, parent: SwiftUIWebView) {
            self.viewModel = viewModel
            self.parent = parent
        }
        
        func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
            
            // The data storage part of the bridge.
            if message.name == "seamlyBridge" {
                // Optionally change this line when using your own data storage.
                // Make sure to only store the message body and store it as-is
                UserDefaults.standard.set(message.body, forKey: "Seamly")
                
                // Communicate the data back to JavaScript
                parent.webView.evaluateJavaScript("window.seamlyBridgeData=(\(message.body)");
            }
        }
    }

    func makeCoordinator() -> SwiftUIWebView.Coordinator {
        Coordinator(viewModel, parent: self)
    }
}
